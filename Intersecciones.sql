﻿
-- c1 estos son los ciclistas que han ganado etapa--
--SELECT DISTINCT dorsal FROM etapa;

-- utilizando vista --

  CREATE OR REPLACE VIEW consulta4c1 AS SELECT DISTINCT dorsal FROM etapa;

SELECT ciclista.dorsal FROM ciclista LEFT JOIN puerto ON ciclista.dorsal = puerto.dorsal WHERE puerto.dorsal IS NULL;

-- c2 ciclistas que han ganado puertos --
  SELECT DISTINCT dorsal FROM puerto;

--utilizando vista --

  CREATE OR REPLACE VIEW consulta4c2 AS SELECT DISTINCT dorsal FROM puerto;

-- c3 ciclistas que no han ganado puertos --
SELECT ciclista.dorsal FROM ciclista LEFT JOIN puerto ON ciclista.dorsal = puerto.dorsal WHERE puerto.dorsal IS NULL;

-- utilizando vista --
CREATE OR REPLACE VIEW consulta4c3 AS SELECT ciclista.dorsal FROM ciclista LEFT JOIN puerto ON ciclista.dorsal = puerto.dorsal WHERE puerto.dorsal IS NULL;
  
-- c1-c3 -- ciclistas que han ganado etapas y que han ganado puertos --
  SELECT * FROM consulta4c1 c1 LEFT JOIN consulta4c3 c3 USING (dorsal) WHERE c3.dorsal IS NULL;
  
  SELECT c1.dorsal FROM consulta4c1 c1 WHERE c1.dorsal NOT IN(SELECT * FROM consulta4c3);

-- c1-c2: ciclistas que han ganado etapas y no han ganado puertos --
  SELECT c1.dorsal FROM consulta4c1 c1 LEFT JOIN consulta4c2 c2 ON c1.dorsal=c2.dorsal WHERE c2.dorsal IS NULL;

  SELECT c1.dorsal FROM consulta4c1 c1 WHERE c1.dorsal NOT IN (SELECT * FROM consulta4c2 c);

-- c1 union c2 : ciclistas que han ganado etapas mas los ciclistas que han ganado puertos--
  SELECT * FROM consulta4c1 c1 UNION SELECT * FROM consulta4c2 c2;

-- c1 union c3: ciclistas que han ganado etapas mas los ciclistas que no han ganado puertos --
  SELECT * FROM consulta4c1 c1 UNION SELECT * FROM consulta4c3 c3;

-- c1 instersccion c3 que es lo mismo que c1-c2( c1 - complementario de c2):ciclistas que han ganado etapas y además no han ganado puerto
SELECT * FROM consulta4c1 c1 NATURAL JOIN consulta4c3 c3;

-- c1 interseccion c2: ciclistas que han ganado etapas y ademas han ganado puertos: c1-c3 --
  SELECT * FROM consulta4c1 c NATURAL JOIN consulta4c2 c1;


-- c1 vista ciclicstas que llevan maillot --

  CREATE OR REPLACE VIEW c1 AS SELECT dorsal FROM lleva;

-- c2 vista ciclistas que no llevan maillot--

  CREATE OR REPLACE VIEW c2 AS SELECT dorsal FROM ciclista LEFT JOIN c1 USING (dorsal) WHERE c1.dorsal IS NULL;

-- c3 vista ciclistas que han ganado etapa--

  CREATE OR REPLACE VIEW c3 AS SELECT dorsal FROM etapa;

-- c4 vista ciclistas que han ganado puerto--

  CREATE OR REPLACE VIEW c4 AS SELECT dorsal FROM puerto;

  -- c3-c1:ciclistas que han ganado etapa y que no llevan maillot --
    SELECT c3.dorsal FROM c3 LEFT JOIN c1 ON c3.dorsal=c1.dorsal WHERE c1.dorsal IS NULL;

 -- c3-c2:ciclistas que han ganado etapa y que llevan maillot--
  SELECT c3.dorsal FROM c3 LEFT JOIN c2 ON c3.dorsal=c2.dorsal WHERE c2.dorsal IS NULL;

  -- c4-c3-c2 ciclistas que han ganado puerto y que no han ganado etapa y que no llevan maillot --
SELECT c4.dorsal FROM c4 LEFT JOIN c3 ON c4.dorsal=c3.dorsal  LEFT JOIN c2 ON c4.dorsal=c2.dorsal WHERE c3.dorsal IS NULL AND c2.dorsal IS NULL;

 -- c1 UNION c3 ciclistas que llevan maillot y que han ganado etapa --
  SELECT dorsal FROM c1 UNION SELECT * FROM  c3;

  -- c1 UNION c3 UNION c4 ciclistas que llevan maillot y que han ganado etapa y que han ganado puerto --
SELECT * FROM c1 UNION SELECT * FROM c3 UNION SELECT * FROM c4;

-- c3 inteserccion c2: ciclistas que han ganado etapa y que han ganado puerto --
  SELECT * FROM c3 NATURAL JOIN c2;

-- c3 interseccion c1: ciclistas que han ganado etapa y que han llevado maillot --
  SELECT DISTINCT* FROM c3 NATURAL JOIN c1;
    
