﻿SELECT * FROM ciclista JOIN puerto ON ciclista.dorsal = puerto.dorsal WHERE altura>1200;

-- c1 --
SELECT DISTINCT p.dorsal FROM puerto p WHERE altura>1200;
--  consulta completa --
SELECT * FROM (SELECT DISTINCT p.dorsal FROM puerto p WHERE altura>1200) AS c1 JOIN ciclista ON c1.dorsal=ciclista.dorsal;

-- utilizamos vistas --
  CREATE or REPLACE VIEW consulta2c1 AS SELECT DISTINCT p.dorsal FROM puerto p WHERE altura>1200;

CREATE OR REPLACE VIEW consulta2 AS SELECT ciclista.* FROM consulta2c1 AS c1 JOIN ciclista ON c1.dorsal=ciclista.dorsal;

-- consulta 3 -- 
  -- ciclistas que no han ganado etapas --

 SELECT ciclista.* FROM ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal WHERE etapa.dorsal IS null;
-- consulta completa --

SELECT ciclista.* FROM ciclista LEFT JOIN (SELECT DISTINCT dorsal FROM etapa) AS c1 ON c1.dorsal=ciclista.dorsal WHERE c1.dorsal IS NULL;