﻿-- Listar el nombre de la persona y el nombre de su supervisor --
      SELECT persona, supervisor FROM supervisa;

--  Indicar el nombre de las personas que trabajan para FAGOR --
  SELECT persona FROM trabaja WHERE compañia='fagor';

-- Indicar el nombre de las personas que trabajan para FAGOR o para INDRA --
   SELECT persona FROM trabaja WHERE compañia='fagor'OR compañia='indra';

--  Indicar el número de ciudades que hay en la tabla ciudades --
  SELECT COUNT(*) FROM ciudad;

--Listar el nombre de las personas y el número de habitantes de la ciudad donde viven--
  SELECT persona.nombre,población FROM ciudad JOIN persona ON ciudad.nombre = persona.ciudad;

-- Listar el nombre de las personas, la calle donde vive y la población de la ciudad donde vive --
  SELECT DISTINCT * FROM persona JOIN ciudad ON ciudad=ciudad.nombre;
  SELECT * FROM ciudad;
  SELECT * FROM persona;