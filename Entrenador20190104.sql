﻿--  Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ANALISTA. Ordenar el resultado por apellido y oficio de forma ascendente --
  SELECT * FROM emple WHERE dept_no=10 and oficio='analista' orDER BY apellido, oficio;

  SELECT * FROM emple;

  -- Realizar un listado de los distintos meses en que los empleados se han dado de alta --
  SELECT DISTINCT MONTH(fecha_alt) FROM  emple ;

  -- Realizar un listado de los distintos años en los que los empleados se han dado de alta --
SELECT DISTINCT YEAR(fecha_alt) FROM emple;

-- Realizar un listado de los distintos días del mes en que los empleados se han dado de alta --
  SELECT DISTINCT DAY (fecha_alt) FROM emple ;

-- Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento número 20 --
  SELECT apellido FROM emple WHERE salario>2000 OR dept_no='20';

--  Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A o por M. Listar el apellido de los empleados --
  SELECT apellido FROM emple WHERE apellido LIKE 'a%' OR apellido LIKE 'm%';

--  Seleccionar de la tabla EMPLE aquellas filas cuyo apellido empiece por A y el oficio tenga una E en cualquier posición. Ordenar la salida por oficio y por salario de forma descendente--
  SELECT * FROM emple WHERE apellido LIKE 'a%' AND oficio LIKE '%e%' ORDER BY oficio , salario DESC;

-- Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A. Listar el apellido de los empleados --
  SELECT apellido FROM emple WHERE apellido LIKE 'a%';
-- Indicar el número de empleados que hay --
  SELECT COUNT(*) frOM emple ;

  -- Indicar el número de departamentos que hay --
    SELECT COUNT(DISTINCT dept_no) FROM emple;

    SELECT COUNT(*) FROM depart;

    