﻿-- Indicar el nombre de las ciudades que tengan una población por encima de la población media
  SELECT * FROM ciudad WHERE población>(SELECT AVG(población) FROM ciudad );
  SELECT AVG(población) FROM ciudad ;
  SELECT nombre FROM ciudad WHERE población>(SELECT AVG(población) FROM ciudad);

  -- Indicar el nombre de las ciudades que tengan una población por debajo de la población media
    SELECT nombre FROM ciudad WHERE población<(SELECT AVG(población) FROM ciudad );

    -- Indicar el nombre de la ciudad con la población máxima
      SELECT MAX(población) FROM ciudad c;
      SELECT DISTINCT nombre FROM ciudad WHERE (SELECT MAX(población) FROM ciudad c)=población;

      SELECT c.nombre FROM ciudad c WHERE c.población=(
        SELECT MAX(población) FROM ciudad c
      );

-- Indicar el nombre de la ciudad con la población mínima 
  SELECT MIN(población) FROM ciudad;
  SELECT nombre FROM ciudad WHERE población=( SELECT MIN(población) FROM ciudad);

  -- Listarme el nombre de la persona, el nombre de su supervisor y las ciudades donde residen cada una de ellos. Ordena por el nombre del supervisado
SELECT nombre,ciudad FROM persona;
SELECT supervisor FROM supervisa;
SELECT p.nombre,p.ciudad FROM persona p JOIN  supervisa ON persona=p.nombre JOIN persona ON c1.supervisor=p.nombre;
SELECT persona,supervisor,p1.ciudad,p2.ciudad
 FROM supervisa JOIN persona p1 ON persona=p1.nombre JOIN persona p2 ON supervisor=p2.nombre ORDER BY persona;

-- Indicar el número de ciudades que tengan una población por encima de la población media 
  SELECT AVG(población) FROM ciudad;
  SELECT COUNT(*) FROM ciudad WHERE población>(SELECT AVG(población) FROM ciudad);

  --  Indicarme el número de personas (de la tabla personas) que viven en cada ciudad. No se refiere a la población total de la ciudad.
    SELECT ciudad,COUNT(*) FROM persona GROUP BY ciudad;

  -- Utilizando la tabla trabaja, indicar cuantas personas trabajan en cada una de las compañías 
    SELECT compañia, COUNT(*) FROM trabaja GROUP BY compañia;

    --  Indicarme el salario medio de cada una de las compañías
      SELECT compañia,AVG(salario) FROM trabaja GROUP BY compañia;

-- Listar el nombre de las personas, la ciudad donde vive y la ciudad donde está la compañía para la que trabaja
  
  SELECT persona,persona.ciudad,compania.ciudad FROM persona JOIN trabaja ON persona.nombre=persona JOIN compania ON compañia=compania.nombre ;