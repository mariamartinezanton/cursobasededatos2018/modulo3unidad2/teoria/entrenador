﻿-- Indicar el nombre de las personas que no trabajan para FAGOR
SELECT persona FROM trabaja WHERE compañia='fagor';
SELECT * FROM persona WHERE NOT IN SELECT persona FROM trabaja WHERE compañia='fagor';
SELECT nombre FROM persona ;
SELECT persona FROM trabaja WHERE (compañia='fagor') c1 LEFT JOIN (SELECT nombre FROM persona) c2  ON c1.nombre=c2.persona;




-- SELECT * FROM persona LEFT JOIN SELECT * FROM compania WHERE nombre='fagor' IS NULL;

ALTER TABLE trabaja ADD FOREIGN KEY(persona) REFERENCES persona(nombre);


SELECT nombre FROM (
    SELECT nombre FROM persona
  ) c2 LEFT JOIN (
    SELECT persona FROM trabaja WHERE compañia='FAGOR'
  ) c1 ON persona=nombre WHERE persona IS NULL;

-- Indicar la compañía que más trabajadores tiene
  SELECT c1.nombre,c2.nombre FROM (SELECT nombre FROM persona GROUP BY ciudad) c1 JOIN (SELECT nombre FROM compania) c2 ON c1.nombre=c2.nombre;

  SELECT compañia,COUNT(persona) AS n  FROM trabaja GROUP BY compañia;
  
  SELECT MAX(c1.n) FROM (sELECT compañia,COUNT(persona) AS n  FROM trabaja GROUP BY compañia) c1 HAVING;

  SELECT compañia FROM trabaja GROUP BY compañia HAVING COUNT(*)=(SELECT MAX(N) FROM (SELECT compañia, COUNT(*)n FROM trabaja GROUP BY compañia)c1);

  -- Consulta todos los datos de todos los programas
    SELECT * FROM programa;

    -- Averigua el DNI de todos los clientes
     SELECT dni FROM cliente ;

    --  Obtén un listado de programas por orden de nombre y versión
      SELECT * FROM programa ORDER BY nombre, version;

      -- Obtén el DNI más 4 de todos los clientes
        SELECT dni+4 FROM cliente ;

     -- Haz un listado con los códigos de los programas multiplicados por 7
      
      SELECT codigo*7 FROM programa; 

      -- ¿Cuáles son todos los datos del programa cuyo código es 11?
        
SELECT * FROM programa WHERE codigo='11';

--  Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre comience por una A o por una W
  SELECT * FROM programa WHERE version LIKE '%i' OR nombre like'A%'or nombre like'W%';

  --  Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, y cuyo nombre no comience por una A
    SELECT * FROM programa WHERE version LIKE '%i'AND  nombre NOT LIKE 'a%';

    -- Obtén un listado con los nombres de todos los programas
      SELECT DISTINCT nombre FROM programa;
      
      --  Genera una lista con todos los comercios
           SELECT DISTINCT nombre FROM comercio;