﻿SELECT * FROM emple;

-- Mostrar todos los campos y todos los registros de la tabla departamento --
SELECT * FROM depart ;

-- Mostrar el número, nombre y localización de cada departamento --
  SELECT dept_no,dnombre,loc FROM depart;

  --  Datos de los empleados ordenados por número de departamento descendentemente --
    SELECT * FROM emple ORDER BY  dept_no DESC;

    -- Datos de los empleados ordenados por número de departamento descendentemente y por oficio ascendente --
      SELECT * FROM emple ORDER BY dept_no DESC, oficio ASC ;

      --  Datos de los empleados ordenados por número de departamento descendentemente y por apellido ascendentemente --
        SELECT * FROM emple ORDER BY dept_no DESC, apellido ASC;

        -- Mostrar el apellido y oficio de cada empleado --
          SELECT DISTINCT apellido, oficio FROM  emple ;

          -- Mostrar localización y número de cada departamento --
SELECT loc,dept_no FROM depart;

  --  Datos de los empleados ordenados por apellido de forma ascendente --
    SELECT * FROM emple ORDER BY apellido;

 --  Datos de los empleados ordenados por apellido de forma descendente --
  SELECT * FROM emple ORDER BY apellido DESC;   

  -- Mostrar los datos de los empleados cuyo oficio sea ANALISTA --
    SELECT * FROM emple WHERE oficio='ANALISTA';

-- Mostrar los datos de los empleados cuyo oficio se ANALISTA y ganen más de 2000 --
  SELECT * FROM emple WHERE oficio='analista' AND salario >2000;

  -- Listar el apellido de todos los empleados y ordenarlos por oficio y por nombre --
    SELECT apellido FROM emple ORDER BY oficio, apellido;

-- Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por Z. Listar todos los campos de la tabla empleados --
  SELECT * FROM emple WHERE apellido NOT LIKE '%z';

  -- Mostrar el código de los empleados cuyo salario sea mayor que 2000 --
    SELECT emp_no FROM emple WHERE salario >2000;

-- Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000 --
   SELECT emp_no, apellido FROM  emple WHERE salario<2000;

  --  Mostrar los datos de los empleados cuyo salario esté entre 1500 y 2500 --
    SELECT * FROM emple WHERE salario BETWEEN 1500 AND 2000 ;

--  Seleccionar el apellido y oficio de los empleados del departamento número 20 --
   SELECT apellido, oficio  FROM  emple WHERE dept_no=20;

-- Mostrar todos los datos de empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendente. --
  SELECT * FROM emple WHERE apellido LIKE 'm%' OR apellido LIKE 'n%' ORDER BY apellido ;

  -- Seleccionar los empleados cuyo oficio sea VENDEDOR. Mostrar los datos ordenados por apellido de forma ascendente. --
SELECT * FROM emple WHERE oficio='vendedor' ORDER BY apellido;